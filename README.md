# Introduction to language theory and compiling Project – Part3

This is the third part of the project, consisting in generating LLVM IR code from Imp programs.



## Statement

For this third and last part of the project, we ask you to augment the recursive-descent LL(1)
parser you have written during the first part to let it generate code that corresponds to the
semantics of the Imp program that is being compiled (those semantics are informally provided
in Appendix B). The output code must be LLVM intermediary language (LLVM IR), which
you studied during the practicals. This code must be accepted by the llvm-as tool, so that is
can be converted to machine code. It is not allowed to first compile to another language (e.g.
C) and then use the language compiler to get LLVM IR code.
When generating the code for expressions, pay extra attention to the associativity and
priority of the operators. The modification of the grammar you have done during part 2 should
help you in that respect.



## Guidelines
Those are only guidelines, and you have entire freedom of implementation for the code generator
(except that you are not allowed to use CUP to generate the code itself, only to obtain the parse
tree if it is difficult to get it directly from your parser).
To generate code from your parser, you can:
1. Modify it to make it generate a parse tree
2. From the parse tree, deduce an Abstract Syntax Tree, which abstracts away some nonterminals
to obtain the very structure of the program. For instance, consider the following
program:
begin
a := 1 + 2 * (3 + 4) / 5
end
Be careful that here, since * and / have the same priority, the parsing is done according
to the left associativity, i.e. the arithmetic expression should be parenthesized2 as 1+(2 
(3 + 4))/5. Its parse tree, depicted in Figure 1, can be converted to the AST of Figure 2
(both figures can be found in Appendix A).
You should draw inspiration from this example to design an abstract representation of
<If>, <For>, <While>, and the like. Note in particular that some non-terminals were
omitted (e.g. ProdExpr and InstTail), since they have no semantic meaning: they are
only dummy non-terminals respectively used to enforce priority and to simulate lists.
3. Walk the AST and generate code. For example, when walking a node Tree1 Tree2 , you
should first evaluate the expression represented by Tree1, then evaluate the expression
represented by Tree2, then compute the result of the addition and store it in an unnamed
variable (whose number should be deduced from the ones used in Tree1 and Tree2). It
should give something of the form:
[sequence of instructions to evaluate Tree1]
[%n is assigned the result of Tree1]
[sequence of instructions to evaluate Tree2, unnamed variables start at n + 1]
[%m is assigned the result of Tree2]
%p3 = %n + %m
Of course, you can also directly generate the code from the parse tree (i.e. avoid step 2), or
obtain the AST directly from the parser (i.e. avoid step 1), or even do everything at once, but
this might be more complex: this decomposition separates the difficulties.

![tree](https://image.noelshack.com/fichiers/2019/46/3/1573656891-substree-syntax.png)
![semantics](https://image.noelshack.com/fichiers/2019/46/3/1573656891-semantics.png)

## Running

The command for running your executable must be:

java -jar part3.jar inputFile

## Design with

_exemples :_
* [Java](https://www.java.com) - Java 1.6 
* [Atom](https://atom.io/) - Editeur de textes
* [JFlex](https://jflex.de/)  - JFlex is a lexical analyzer generator  (1.6.1)


## Authors
Listez le(s) auteur(s) du projet ici !
* **Sofiane Madrane** _alias_ [@sofiane_madrane](https://gitlab.com/sofiane_madrane)




