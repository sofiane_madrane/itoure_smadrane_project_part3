declare i32 @printf(i8*, ...)
@string = private unnamed_addr constant [4 x i8] c"%d\0A\00"

declare i32 @getchar ()
define i32 @readInt(){
entry:
  %res = alloca i32
  %digit = alloca i32
  %negatif = alloca i1
  store i32 0 , i32 * %res
  store i1 0, i1 * %negatif
  br label %read
read:
  %0 = call i32 @getchar ()
  %1 = sub i32 %0 , 48
  store i32 %1 , i32 * %digit
  %2 = icmp eq i32 %0 , 45
  br i1 %2 , label %negatifNumber , label %next
negatifNumber:
  %3 = add i1 0, 1
  store i1 %3, i1 * %negatif
  br label %read
next:
  %4 = icmp ne i32 %0 , 10
  br i1 %4 , label %save , label %exit
save:
  %5 = load i32, i32 * %res
  %6 = load i32, i32 * %digit
  %7 = mul i32 %5 , 10
  %8 = add i32 %7 , %6
  store i32 %8 , i32 * %res
  br label %read
exit:
  %9 = load i1, i1 * %negatif
  br i1 %9 , label %transform , label %return
transform:
  %10 = load i32, i32 * %res
  %11 = sub i32 0, %10
  store i32 %11 , i32 * %res
  br label %return
return:
  %12 = load i32, i32 * %res
  ret i32 %12
}

define i32 @main(){
	%1 = call i32 @readInt ()
	store i32 %1 , i32 * %_a
	%2 = call i32 @readInt ()
	store i32 %2 , i32 * %_b
	%3 = load i32, i32* %_b
	%4 = add i32 0, 0
	%5 = icmp ne i32 %3, %4
	br i1 %5 , label %while0 , label %do0
while0:
	%_c = alloca i32
	%6 = load i32, i32* %_b
	store i32 %6, i32* %_c
	%_a = alloca i32
	%7 = load i32, i32* %_a
	%8 = load i32, i32* %_b
	%9 = sub i32 %7, %8
	store i32 %9, i32* %_a
	br label %while0
while0:
	%_b = alloca i32
	%10 = load i32, i32* %_a
	store i32 %10, i32* %_b
	%11 = load i32, i32* %_c
	store i32 %11, i32* %_a
	%12 = load i32, i32* %_a
	%13 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @string, i32 0, i32 0), i32 %_a)
	ret i32 0
}
