/**
 * This class an thrown exception when the compiler encounters an error
 *
 * @author S.Madrane, I.Toure
 * @version 1.0
 */

public class CompilerException extends java.io.IOException {

    /**
     * Displays the error message due to the exception.
     */
    public CompilerException() {
        super("[CompilerException] There exists syntax errors in the input file, aborting the compilation...");
    }


    /**
     * Displays the error message due to the exception.
     *
     * @param indexRules representing the error produced.
     */
    public CompilerException(int indexRules) {
        super("[CompilerException] A syntax error occurs at rule [" + indexRules + "]");
    }

    /**
     * Displays the error message due to the exception
     *
     * @param message message about the error
     */
    public CompilerException(String message) {
        super("[CompilerException] There exists syntax errors in the input file, aborting the compilation... "+message);
    }
}
