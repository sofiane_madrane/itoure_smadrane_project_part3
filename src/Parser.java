import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

/**
 * This class represents the logic of parser.
 *
 * @author  I.Toure, S.Madrane,
 * @version 1.0
 */
public class Parser {

    private final ArrayList<Symbol> tokenTable ;
    // Index on the List of token of the current one
    private int indexCurrentToken ;
    private Symbol currentToken ;

    //part llvm
    private final Map<String,Integer> variables ;
    private int lastID ;
    private int counter ;
    private String fileName;

    private int idCondition ;    // for nested if and if-else statements
    private int idLoop ;         // for nested loops statements

    private final LlvmGenerator llvmGenerator ;

    /**
     * Constructor of the Parser
     * @param tokenTable List of token of the input file
     * @param fileName Name of the file
     */
    public Parser(ArrayList<Symbol> tokenTable, String fileName) {
        this.indexCurrentToken = 0 ;
        this.tokenTable = tokenTable ;
        this.lastID = 0 ;
        this.counter = -1 ;
        this.idCondition = 0 ;
        this.idLoop = 0 ;
        this.variables	= new HashMap<String,Integer>() ;
        this.llvmGenerator = new LlvmGenerator() ;
        this.fileName = fileName;
        if (this.fileName != null) {
            this.llvmGenerator.setFileName(fileName);
            this.fileName = fileName;
        }
    }

    /**
     * Parse the token of the input file
     * @throws CompilerException if a syntax error happen during the parsing of the input file
     */
    public void parse() throws CompilerException {
        if (!tokenTable.isEmpty()) {
            currentToken = tokenTable.get(indexCurrentToken);
            Program();
            System.out.println("The input file is syntactically correct");

            if (this.fileName == null)
                llvmGenerator.showResult();
            else
                llvmGenerator.createLlvmFile();

        } else {
            throw new CompilerException(1);
        }
    }

    /**
     * Create new variable in llvm file
     * @param varname name of the variable
     * @throws CompilerException if variable was already created
     */
    private void create(String varname) throws CompilerException {
        final String privateName = "_" + varname;
        if(variables.containsKey(privateName)){
            throw new CompilerException("[CompilerException] Error: redeclaration of variable \""+varname+"\" at line "+lineLastMatch());
        }
        variables.put(privateName,new Integer(++counter));
    }

    /**
     * Check if a variable has been created before use
     * @param varname name of variable
     * @throws CompilerException if variable was not created
     */
    private boolean check(String varname) throws CompilerException {
        final String privateName = "_" + varname;
        /*if(!variables.containsKey(privateName)){
            throw new CompilerException("[CompilerException] Error: Undeclared variable \""+varname+"\" at line "+lineLastMatch());
        }*/
        return variables.containsKey(privateName);
    }

    /**
     * Match the current token LexicalUnit with the one that has been derivated
     * from the rules of the grammar
     * @param unit LexicalUnit that has been derivated from the grammar
     * @throws CompilerException if any LexicalUnit mismatch happen
     */
    private void match(LexicalUnit unit) throws CompilerException {
        if (indexCurrentToken == tokenTable.size()) {
            throw new CompilerException("match1");
        }
        if (currentToken.getType() != unit) {
            throw new CompilerException("match2");
        } else {
            indexCurrentToken++;
            if (indexCurrentToken < tokenTable.size()) {
                currentToken = tokenTable.get(indexCurrentToken);
            }
        }
    }

    /**
     * Return the current variable of the Program
     * @return the current variable of the Program
     */
    private String valLastMatch(){
        return (String)this.tokenTable.get(this.indexCurrentToken-1).getValue().toString();
    }

    /**
     * Return the current line of the Program
     * @return the current line of the Program
     */
    private int lineLastMatch(){
        return this.tokenTable.get(this.indexCurrentToken-1).getLine();
    }

    /**
     * Method to move to the next variable
     * @return ID for the next variable
     */
    private String nextVariable(){
        return (++lastID)+"";
    }


    /**
     * Represent the variable Program of the grammar
     * @throws CompilerException if any syntax error happen on the current rule
     */
    private void Program() throws CompilerException {
        switch (currentToken.getType()) {
            case BEGIN:
                match(LexicalUnit.BEGIN);
                llvmGenerator.startMainFunction();
                Code();
                match(LexicalUnit.END);
                llvmGenerator.endMainFunction();
                if (indexCurrentToken != tokenTable.size()) {
                    throw new CompilerException("[CompilerException] "
                            + "There exists syntax errors in the input file, the token END is not "
                            + "the last token of the input file.\nAborting the compilation...");
                }
                break;
            default:
                throw new CompilerException(1);
        }
    }

    /**
     * Represent the variable Code of the grammar
     * @throws CompilerException if any syntax error happen on the current rule
     */
    private void Code() throws CompilerException {
        switch (currentToken.getType()) {
            case END:
            case ENDIF:
            case ELSE:
            case DONE:
                //printRuleString(2);
                break;
            case VARNAME:
            case IF:
            case WHILE:
            case FOR:
            case PRINT:
            case READ:
                //printRuleString(3);
                InstList(currentToken.getType());
                //Code();
                //match(LexicalUnit.END);
                break;
            default:
                throw new CompilerException("code");
        }
    }

    /**
     * Represent the variable InstList of the grammar
     * @throws CompilerException if any syntax error happen on the current rule
     */
    private void InstList(LexicalUnit unitCaller) throws CompilerException {
        switch (currentToken.getType()) {
            case VARNAME:
            case WHILE:
            case PRINT:
            case IF:
            case READ:
            case FOR:
                //printRuleString(4);
                Instruction();
                InstListPrime(unitCaller);
                break;
            default:
                throw new CompilerException("InstList");
        }
    }

    /**
     * Represent the variable InstListPrime of the grammar
     * @throws CompilerException if any syntax error happen on the current rule
     */
    private void InstListPrime(LexicalUnit unitCaller) throws CompilerException {
        switch (currentToken.getType()){
            case SEMICOLON:
                match(LexicalUnit.SEMICOLON);
                InstList(unitCaller);
                break;
            case END:
            case ENDIF:
            case ELSE:
            case DONE:
                //printRuleString();
                break;
            default:
                throw new CompilerException("InstListPrime");
        }
    }

    /**
     * Represent the variable Instruction of the grammar
     * @throws CompilerException if any syntax error happen on the current rule
     */
    private void Instruction() throws CompilerException {
        switch (currentToken.getType()) {
            case VARNAME:
                Assign();
                break;
            case WHILE:
                While();
                break;
            case PRINT:
                Print();
                break;
            case IF:
                If();
                break;
            case READ:
                Read();
                break;
            case FOR:
                For();
                break;
            default:
                throw new CompilerException("Instruction");
        }
    }

    /**
     * Represent the variable Assign of the grammar
     * @throws CompilerException if any syntax error happen on the current rule
     */
    private void Assign() throws CompilerException {
        switch (currentToken.getType()){
            case VARNAME:
                //printRuleString(13);
                match(LexicalUnit.VARNAME);
                String variable = valLastMatch();

                if(!check(variable)) {
                    create(variable);
                    llvmGenerator.declareVariable(variable);
                }

                match(LexicalUnit.ASSIGN);
                String tempVar = ExprArith();
                llvmGenerator.assignStatement(tempVar,variable);
                break;
            default:
                throw new CompilerException("Assign");
        }
    }

    /**
     * Represent the variable ExprArith of the grammar
     * @throws CompilerException if any syntax error happen on the current rule
     */
    private String ExprArith() throws CompilerException {
        String varCodeTmp = "";
        switch (currentToken.getType()) {
            case VARNAME:
            case MINUS:
            case NUMBER:
            case LPAREN:
                //printRuleString(14);
                varCodeTmp = Prod(varCodeTmp);
                varCodeTmp = ExprArithPrime(varCodeTmp);
                break;
            default:
                throw new CompilerException("ExprArith");
        }
        return varCodeTmp;
    }

    /**
     * Represent the variable ExprArithPrime of the grammar
     * @throws CompilerException if any syntax error happen on the current rule
     */
    private String ExprArithPrime(String tempVarOperand) throws CompilerException {
        String tempVar = tempVarOperand;
        String tempVarOperandRes;
        switch (currentToken.getType()){
            case PLUS:
                match(LexicalUnit.PLUS);
                tempVarOperandRes = Prod(tempVarOperand);
                tempVar = nextVariable();
                llvmGenerator.plusStatement(tempVar, tempVarOperand, tempVarOperandRes);
                tempVar = ExprArithPrime(tempVar);
                break;
            case MINUS:
                match(LexicalUnit.MINUS);
                tempVarOperandRes = Prod(tempVarOperand);
                tempVar = nextVariable();
                llvmGenerator.minusStatement(tempVar,tempVarOperand,tempVarOperandRes);
                tempVar = ExprArithPrime(tempVar);
                break;
            case SEMICOLON:
            case LPAREN:
            case RPAREN:
            case THEN:
            case END:
            case ENDIF:
            case ELSE:
            case OR:
            case AND:
            case EQ:
            case GEQ:
            case GT:
            case LEQ:
            case LT:
            case NEQ:
            case DO:
            case DONE:
            case BY:
            case TO:
                // Verifier printRuleString(17);
                break;
            default:
                throw new CompilerException("ExprArithPrime");
        }
        return tempVar;
    }

    /**
     * Represent the variable Prod of the grammar
     * @param varCodeCaller Code caller variable
     * @throws CompilerException if any syntax error happen on the current rule
     */
    private String Prod(String varCodeCaller) throws CompilerException {
        String varCodeTmp = varCodeCaller;
        switch (currentToken.getType()) {
            case VARNAME:
            case MINUS:
            case NUMBER:
            case LPAREN:
                varCodeTmp = Atom();
                varCodeTmp = ProdPrime(varCodeTmp);
                break;
            default:
                throw new CompilerException("Prod");
        }
        return varCodeTmp;
    }

    /**
     * Represent the variable ProdPrime of the grammar
     * @param tempVarOperand  temporary operand variable
     * @throws CompilerException if any syntax error happen on the current rule
     */
    private String ProdPrime(String tempVarOperand) throws CompilerException {
        String tempVar = tempVarOperand;
        String tempVarOperandRes;
        switch (currentToken.getType()){
            case TIMES:
                match(LexicalUnit.TIMES);
                tempVarOperandRes = Atom();
                tempVar = nextVariable();
                llvmGenerator.timesStatement(tempVar, tempVarOperand, tempVarOperandRes);
                tempVar = ProdPrime(tempVar);
                break;
            case DIVIDE:
                match(LexicalUnit.DIVIDE);
                tempVarOperandRes = Atom();
                tempVar = nextVariable();
                llvmGenerator.devideStatement(tempVar,tempVarOperand,tempVarOperandRes);
                tempVar = ProdPrime(tempVar);
                break;
            case MINUS:
            case PLUS:
            case ENDIF:
            case ELSE:
            case OR:
            case AND:
            case EQ:
            case GEQ:
            case GT:
            case LEQ:
            case LT:
            case NEQ:
            case BY:
            case TO:
            case RPAREN:
            case END:
            case DONE:
            case SEMICOLON:
            case DO:
            case THEN:
                //printRuleString(21);
                break;

            default:
                throw new CompilerException("ProdPrime");
        }
        return tempVar;
    }

    /**
     * Represent the variable Atom of the grammar
     *
     * @throws CompilerException if any syntax error happen on the current rule
     */
    private String Atom() throws CompilerException {
        String tempVar = "", codeVariable;
        switch (currentToken.getType()) {
            case VARNAME:
                match(LexicalUnit.VARNAME);
                if(!check(valLastMatch())) //Possible enlever
                    throw new CompilerException("Varname "+valLastMatch()+" is'nt declared!");
                codeVariable = "_"+valLastMatch();
                tempVar = nextVariable();
                llvmGenerator.createCopyVarStatement(tempVar, codeVariable);
                break;
            case MINUS:
                match(LexicalUnit.MINUS);
                String varTmpRes = Atom();
                tempVar = nextVariable();
                llvmGenerator.createNegativeNumberStatement(tempVar,varTmpRes);
                break;
            case NUMBER:
                match(LexicalUnit.NUMBER);
                tempVar = nextVariable();
                llvmGenerator.createNumberStatement(tempVar,valLastMatch());
                break;
            case LPAREN:
                match(LexicalUnit.LPAREN);
                tempVar = ExprArith();
                match(LexicalUnit.RPAREN);
                break;
            default:
                throw new CompilerException("Atom");
        }
        return tempVar;
    }

    /**
     * Represent the variable If of the grammar
     *
     * @throws CompilerException if any syntax error happen on the current rule
     */
    private void If() throws CompilerException {
        String tempVar = "", boolVariable;
        switch (currentToken.getType()){
            case IF:
                match(LexicalUnit.IF);
                boolVariable = Cond();
                match(LexicalUnit.THEN);
                int idCondition = this.idCondition;
                this.idCondition++;
                llvmGenerator.addIfCondition(boolVariable, idCondition);
                Code();
                IfPrime(idCondition);
                break;
            default:
                throw new CompilerException("If");
        }
    }

    /**
     * Represent the variable IfPrime of the grammar
     * @throws CompilerException if any syntax error happen on the current rule
     */
    private void IfPrime(int idCondition) throws CompilerException {
        switch (currentToken.getType()) {
            case ENDIF:
                match(LexicalUnit.ENDIF);
                llvmGenerator.addEndifCondition(idCondition);
                break;
            case ELSE:
                match(LexicalUnit.ELSE);
                llvmGenerator.addElseCondition(idCondition);
                Code();
                match(LexicalUnit.ENDIF);
                llvmGenerator.addEndifElseCondition(idCondition);
                break;
            default:
                throw new CompilerException("IfPrime");
        }
    }

    /**
     * Represent the variable Cond of the grammar
     *
     * @throws CompilerException if any syntax error happen on the current rule
     */
    private String Cond() throws CompilerException {
        String varCodeTmp, varTmpRes;
        switch (currentToken.getType()){
            case VARNAME:
            case MINUS:
            case NUMBER:
            case RPAREN:
            case NOT:
                varTmpRes = BooleanTerm();
                varCodeTmp = CondPrime(varTmpRes);
                break;
            default:
                throw new CompilerException("Cond");
        }
        return varCodeTmp;
    }

    /**
     * Represent the variable CondPrime of the grammar
     *
    * @throws CompilerException if any syntax error happen on the current rule
     */
    private String CondPrime(String tempVarOperand) throws CompilerException {
        String tempVar = tempVarOperand, tempVarOperand2;
        switch (currentToken.getType()) {
            case OR:
                match(LexicalUnit.OR);
                tempVarOperand2 = BooleanTerm();
                tempVar = nextVariable();
                llvmGenerator.addOrStatement(tempVar, tempVarOperand, tempVarOperand2);
                tempVar = CondPrime(tempVar);
                break;
            case DO:
            case THEN:
                //printRuleString(31);
                break;
            default:
                throw new CompilerException("CondPrime");
        }
        return  tempVar;
    }

    /**
     * Represent the variable BooleanTerm of the grammar
     *
    * @throws CompilerException if any syntax error happen on the current rule
     */
    private String BooleanTerm() throws CompilerException {
        String varCodeTmp, varTmpRes;
        switch (currentToken.getType()){
            case VARNAME:
            case MINUS:
            case NUMBER:
            case RPAREN:
            case NOT:
                varTmpRes = BooleanFactor();
                varCodeTmp = BooleanTermPrime(varTmpRes);
                break;
            default:
                throw new CompilerException("BooleanTerm");
        }
        return varCodeTmp;
    }

    /**
     * Represent the variable BooleanTermPrime of the grammar
     * @param tempVarOperand id of temporary operand variable
     * @throws CompilerException if any syntax error happen on the current rule
     */
    private String BooleanTermPrime(String tempVarOperand) throws CompilerException {
        String tempVar = tempVarOperand, tempVarOperand2;
        switch (currentToken.getType()) {
            case OR:
            case DO:
            case THEN:
                //printRuleString(34);
                break;
            case AND:
                match(LexicalUnit.AND);
                tempVarOperand2  = BooleanFactor();
                tempVar = nextVariable();
                llvmGenerator.addAndStatement(tempVar,tempVarOperand,tempVarOperand2);
                tempVar = BooleanTermPrime(tempVar);
                break;
            default:
                throw new CompilerException("BooleanTermPrime");
        }
        return tempVar;
    }

    /**
     * Represent the variable BooleanFactor of the grammar
     * @throws CompilerException if any syntax error happen on the current rule
     */
    private String BooleanFactor() throws CompilerException {
        String tempVar = "", tempVarOperand;
        switch (currentToken.getType()){
            case NOT:
                match(LexicalUnit.NOT);
                tempVarOperand = SimpleCond();
                tempVar = nextVariable();
                llvmGenerator.addNotStatement(tempVar, tempVarOperand);
                break;
            case VARNAME:
            case MINUS:
            case NUMBER:
            case RPAREN:
                //printRuleString(36);
                tempVar = SimpleCond();
                break;
            default:
                throw new CompilerException("BooleanFactor");
        }
        return tempVar;
    }

    /**
     * Represent the variable SimpleCond of the grammar
     *
     * @throws CompilerException if any syntax error happen on the current rule
     */
    private String SimpleCond() throws CompilerException {
        String varTemp = "", tempVarOperand, tempVarOperand2, comparator;
        switch (currentToken.getType()) {
            case VARNAME:
            case MINUS:
            case NUMBER:
            case LPAREN:
                tempVarOperand = ExprArith();
                comparator = Comp();
                tempVarOperand2 = ExprArith();
                varTemp = nextVariable();
                llvmGenerator.addSimpleCond(varTemp, comparator, tempVarOperand, tempVarOperand2);
                break;
            default:
                throw new CompilerException("SimpleCond");
        }
        return  varTemp;
    }

    /**
     * Represent the variable Comp of the grammar
     *
     * @throws CompilerException if any syntax error happen on the current rule
     */
    private String Comp() throws CompilerException {
        String compOperation = "";
        switch (currentToken.getType()){
            case EQ:
                match(LexicalUnit.EQ);
                compOperation = "eq";
                break;
            case GEQ:
                match(LexicalUnit.GEQ);
                compOperation = "sge";
                break;
            case GT:
                match(LexicalUnit.GT);
                compOperation = "sgt";
                break;
            case LEQ:
                compOperation = "sle";
                match(LexicalUnit.LEQ);
                break;
            case LT:
                compOperation = "slt";
                match(LexicalUnit.LT);
                break;
            case NEQ:
                compOperation = "ne";
                match(LexicalUnit.NEQ);
                break;
            default:
                throw new CompilerException("Comp");
        }
        return compOperation;
    }

    /**
     * Represent the variable While of the grammar
     * @throws CompilerException if any syntax error happen on the current rule
     */
    private void While() throws CompilerException {
        String condVar;
        int idLoop;
        switch (currentToken.getType()) {
            case WHILE:
                match(LexicalUnit.WHILE);
                condVar = Cond();
                match(LexicalUnit.DO);
                idLoop =  this.idLoop;
                this.idLoop++;
                llvmGenerator.addWhileStatement(condVar,idLoop);
                Code();
                match(LexicalUnit.DONE);
                llvmGenerator.endWhileStatement(idLoop);
                break;
            default:
                throw new CompilerException("While");
        }
    }

    /**
     * Represent the variable ForPrime of the grammar
     * @param idLoop identifier of the for loop in llvm file (label match)
     * @param iterator iterator declared variable as iterator of the for statement
     * @param exprVar start number of the iterator
     * @param idCodeVar id iterator
     * @param idVarExpr id number of the iterator
     * @throws CompilerException if any syntax error happen on the current rule
     */
    private void ForPrime(int idLoop,String iterator, String exprVar, String idCodeVar, String idVarExpr) throws CompilerException {
        String exprVar1, exprVar2,tempVar1,tempVar2;
        switch (currentToken.getType()) {
            case BY:
                match(LexicalUnit.BY);
                exprVar1 = ExprArith();
                match(LexicalUnit.TO);
                exprVar2 = ExprArith();
                match(LexicalUnit.DO);
                llvmGenerator.addForStatement(iterator,exprVar,exprVar2,idCodeVar,idVarExpr,idLoop);
                Code();
                match(LexicalUnit.DONE);
                tempVar1 = nextVariable();
                tempVar2 = nextVariable();
                llvmGenerator.addEndForByStatement(iterator,tempVar1,tempVar2 ,exprVar1, idLoop);
                break;
            case TO:
                match(LexicalUnit.TO);
                exprVar1 = ExprArith();
                match(LexicalUnit.DO);
                llvmGenerator.addForStatement(iterator,exprVar,exprVar1,idCodeVar,idVarExpr,idLoop);
                Code();
                match(LexicalUnit.DONE);
                tempVar1 = nextVariable();
                tempVar2 = nextVariable();
                llvmGenerator.addEndForStatement(iterator,tempVar1,tempVar2,idLoop);
                break;
            default:
                throw new CompilerException("ForPrime");
        }
    }

    /**
     * Represent the variable For of the grammar
     *
     * @throws CompilerException if any syntax error happen on the current rule
     */
    private void For() throws CompilerException {
        String exprVar="", tempVar, codeVariable,variable, tempVarExpr;
        int loopCondition ;
        switch (currentToken.getType()){
            case FOR:
                match(LexicalUnit.FOR);
                match(LexicalUnit.VARNAME);
                if(check(valLastMatch())) { // Parler avec ibra creation de var
                    codeVariable = "_"+valLastMatch();
                    tempVar = nextVariable();
                    llvmGenerator.createCopyVarStatement(tempVar, codeVariable);
                    tempVar = nextVariable();
                } else {
                    variable = valLastMatch();
                    codeVariable = "_"+variable;
                    create(valLastMatch());
                    llvmGenerator.declareVariable(variable);
                    //throw new CompilerException("for");
                }
                //codeVariable = "_"+valLastMatch(); // var uniquement dispo dans le for
                tempVar = nextVariable();
                loopCondition = this.idLoop ;
                this.idLoop++;
                match(LexicalUnit.FROM);
                exprVar = ExprArith();
                tempVarExpr = nextVariable();
                ForPrime(loopCondition,codeVariable,exprVar,tempVar,tempVarExpr);
                break;
            default:
                throw new CompilerException("For2");
        }
    }

    /**
     * Represent the variable Print of the grammar
     *
     * @throws CompilerException if any syntax error happen on the current rule
     */
    private void Print() throws CompilerException {
        String codeVariable, tempVar="";
        switch (currentToken.getType()){
            case PRINT:
                match(LexicalUnit.PRINT);
                match(LexicalUnit.LPAREN);
                match(LexicalUnit.VARNAME);
                if(check(valLastMatch())){
                    codeVariable = "_"+valLastMatch();
                    tempVar = nextVariable();
                    llvmGenerator.createCopyVarStatement(tempVar, codeVariable);
                    tempVar = nextVariable();
                }else {
                    throw new CompilerException("Print"); // A changer
                }
                llvmGenerator.addPrintStatement(tempVar,codeVariable);
                match(LexicalUnit.RPAREN);
                break;
            default:
                throw new CompilerException("Print2");
        }
    }

    /**
     * Represent the variable Read of the grammar
     *
     * @throws CompilerException if any syntax error happen on the current rule
     */
    private void Read() throws CompilerException {
        switch (currentToken.getType()) {
            case READ:
                match(LexicalUnit.READ);
                match(LexicalUnit.LPAREN);
                match(LexicalUnit.VARNAME);
                if (!check(valLastMatch())) {
                    String storeVar = valLastMatch();
                    String tempVar = nextVariable();
                    llvmGenerator.addReadStatement(tempVar, storeVar);
                    create(storeVar); // On suppose que chaque var se trouvant dans read est crée et assigné par llvm
                } else {
                    throw new CompilerException("Read"); // A changer
                }
                match(LexicalUnit.RPAREN);
                break;
            default:
                throw new CompilerException("Read2");
        }
    }
}

