import java.util.TreeMap;
import java.util.Map;
import java.util.ArrayList;
%%

%class LexicalAnalyzer
%unicode
%line
%function next_token
%column
%type Symbol
%standalone

%{
	private boolean eof = false;
	boolean eof(){
		return eof;
	}
	Map <String,Integer> symbolTable = new TreeMap<String, Integer>();
	public ArrayList<Symbol> tokenTable = new ArrayList<Symbol>();

	private void createSymbol(int line, int column, LexicalUnit lexUnit, Object value){
		Symbol s = new Symbol(lexUnit, line, column, value);
		tokenTable.add(s);
		System.out.println(s.toString());
		if(s.getType() == LexicalUnit.VARNAME){
			addIdentifier((String)s.getValue(),s.getLine());
		}
	}
	private void addIdentifier(String key,int line){
		if (!symbolTable.containsKey(key)){
			symbolTable.put(key,line);
		}
	}
	public ArrayList<Symbol> getTokenTable(){
		return tokenTable;
	}

%}

%eofval{
	//Output symbol table
	System.out.println();
	System.out.println("Identifiers");
	for(String e : symbolTable.keySet()){
		System.out.println(e+" "+symbolTable.get(e));
	}
	this.eof = true;
	return new Symbol(LexicalUnit.EOS, yyline, yycolumn);

%eofval}

// Extended Regular Expressions

AlphaUpperCase 	= [A-Z]
AlphaLowerCase 	= [a-z]
Alpha          	= {AlphaUpperCase}|{AlphaLowerCase}
Numeric        	= [0-9]
AlphaNumeric 	= {Alpha}|{Numeric}
Assign			= ":="
Number         	= (([1-9][0-9]*)|0)
EndOfLine      	= "\r"|"\n"|"\r\n"
Begin      	   	= "begin"
End 		   	= "end"
Comment			= "(*"(.|{EndOfLine})*"*)"
VarName  	   	= {Alpha}{AlphaNumeric}*
If 				= "if"
Then			= "then"
EndIf 			= "endif"
Else       		= "else"
Not				= "not"
And				= "and"
Or 				= "or"
While			= "while"
Do				= "do"
Done			= "done"
For				= "for"
From			= "from"
By				= "by"
To 				= "to"
Print			= "print"
Read			= "read"
LPar			= "("
RPar			= ")"
SemiColon		= ";"



%%// Identification of tokens
{Comment}			{}
{Begin}				{createSymbol(yyline,yycolumn,LexicalUnit.BEGIN,yytext());}
{End}				{createSymbol(yyline,yycolumn,LexicalUnit.END,yytext());}
{SemiColon}			{createSymbol(yyline,yycolumn,LexicalUnit.SEMICOLON,yytext());}
{Assign}			{createSymbol(yyline,yycolumn,LexicalUnit.ASSIGN,yytext());}
{Number}			{createSymbol(yyline,yycolumn,LexicalUnit.NUMBER,yytext());}
//Op
"+"					{createSymbol(yyline,yycolumn,LexicalUnit.PLUS,yytext());}
"-"					{createSymbol(yyline,yycolumn,LexicalUnit.MINUS,yytext());}
"*"					{createSymbol(yyline,yycolumn,LexicalUnit.TIMES,yytext());}
"/"					{createSymbol(yyline,yycolumn,LexicalUnit.DIVIDE,yytext());}

//Comp
"=" 				{createSymbol(yyline,yycolumn,LexicalUnit.EQ,yytext());}
">=" 				{createSymbol(yyline,yycolumn,LexicalUnit.GEQ,yytext());}
">" 				{createSymbol(yyline,yycolumn,LexicalUnit.GT,yytext());}
"<=" 				{createSymbol(yyline,yycolumn,LexicalUnit.LEQ,yytext());}
"<" 				{createSymbol(yyline,yycolumn,LexicalUnit.LT,yytext());}
"<>" 				{createSymbol(yyline,yycolumn,LexicalUnit.NEQ,yytext());}

//If
{If}				{createSymbol(yyline,yycolumn,LexicalUnit.IF,yytext());}
{Then}				{createSymbol(yyline,yycolumn,LexicalUnit.THEN,yytext());}
{EndIf}				{createSymbol(yyline,yycolumn,LexicalUnit.ENDIF,yytext());}
{Else}				{createSymbol(yyline,yycolumn,LexicalUnit.ELSE,yytext());}
{Not}				{createSymbol(yyline,yycolumn,LexicalUnit.NOT,yytext());}
{And}				{createSymbol(yyline,yycolumn,LexicalUnit.AND,yytext());}
{Or}				{createSymbol(yyline,yycolumn,LexicalUnit.OR,yytext());}
{While}				{createSymbol(yyline,yycolumn,LexicalUnit.WHILE,yytext());}
{Do}				{createSymbol(yyline,yycolumn,LexicalUnit.DO,yytext());}
{Done}				{createSymbol(yyline,yycolumn,LexicalUnit.DONE,yytext());}
{For}				{createSymbol(yyline,yycolumn,LexicalUnit.FOR,yytext());}
{From}				{createSymbol(yyline,yycolumn,LexicalUnit.FROM,yytext());}
{By}				{createSymbol(yyline,yycolumn,LexicalUnit.BY,yytext());}
{To}				{createSymbol(yyline,yycolumn,LexicalUnit.TO,yytext());}
{Print}				{createSymbol(yyline,yycolumn,LexicalUnit.PRINT,yytext());}
{Read}				{createSymbol(yyline,yycolumn,LexicalUnit.READ,yytext());}
{LPar}				{createSymbol(yyline,yycolumn,LexicalUnit.LPAREN,yytext());}
{RPar}				{createSymbol(yyline,yycolumn,LexicalUnit.RPAREN,yytext());}
{VarName}			{createSymbol(yyline,yycolumn,LexicalUnit.VARNAME,yytext());}
{EndOfLine} 		{}
.       			{}