import java.io.FileOutputStream;
import java.io.File;
import java.io.OutputStreamWriter;
import java.io.BufferedWriter;
import java.io.PrintWriter;

/**
 * Class to generate LLVM code.
 * @author  I.Toure, S.Madrane,
 * @version 1.0
 */
public class LlvmGenerator {
    private final String funcRead = "declare i32 @getchar ()\n"
            +"define i32 @readInt(){\n"
            +"entry:\n"
            +"  %res = alloca i32\n"
            +"  %digit = alloca i32\n"
            +"  %negatif = alloca i1\n"
            +"  store i32 0 , i32 * %res\n"
            +"  store i1 0, i1 * %negatif\n"
            +"  br label %read\n"
            +"read:\n"
            +"  %0 = call i32 @getchar ()\n"
            +"  %1 = sub i32 %0 , 48\n"
            +"  store i32 %1 , i32 * %digit\n"
            +"  %2 = icmp eq i32 %0 , 45\n"
            +"  br i1 %2 , label %negatifNumber , label %next\n"
            +"negatifNumber:\n"
            +"  %3 = add i1 0, 1\n"
            +"  store i1 %3, i1 * %negatif\n"
            +"  br label %read\n"
            +"next:\n"
            +"  %4 = icmp ne i32 %0 , 10\n"
            +"  br i1 %4 , label %save , label %exit\n"
            +"save:\n"
            +"  %5 = load i32, i32 * %res\n"
            +"  %6 = load i32, i32 * %digit\n"
            +"  %7 = mul i32 %5 , 10\n"
            +"  %8 = add i32 %7 , %6\n"
            +"  store i32 %8 , i32 * %res\n"
            +"  br label %read\n"
            +"exit:\n"
            +"  %9 = load i1, i1 * %negatif\n"
            +"  br i1 %9 , label %transform , label %return\n"
            +"transform:\n"
            +"  %10 = load i32, i32 * %res\n"
            +"  %11 = sub i32 0, %10\n"
            +"  store i32 %11 , i32 * %res\n"
            +"  br label %return\n"
            +"return:\n"
            +"  %12 = load i32, i32 * %res\n"
            +"  ret i32 %12\n"
            +"}\n\n";

    private final String funcPrint = "declare i32 @printf(i8*, ...)\n"
            + "@string = private unnamed_addr constant [4 x i8] c\"%d\\0A\\00\"\n\n";

    private StringBuilder codeResult;
    private String fileName;

    private boolean funcReadAdded;  // avoid to add function read multiple times
    private boolean funcPrintAdded; // avoid to add function print multiple times


    /**
     * Default construtor
     */
	public LlvmGenerator(){
        this.codeResult = new StringBuilder("") ;
        this.fileName = null ;
	}

    /**
     * create the llvm file
     */
    public void createLlvmFile() {
        boolean autoFlush = true;
        String charset = "UTF-8";
        System.out.println("[CodeGenerator] Generating llvm file... ");
        try {
            File file = new File(this.fileName);
            if (!file.exists()) {
                file.createNewFile();
            }
            FileOutputStream fos = new FileOutputStream(file);
            OutputStreamWriter osw = new OutputStreamWriter(fos, charset);
            BufferedWriter bw = new BufferedWriter(osw);
            PrintWriter pw = new PrintWriter(bw, autoFlush);
            pw.write(codeResult.toString());
            pw.close();
        } catch (Exception e) {
            System.out.println("Error : Can not create ouput file generated \""+fileName+"\"");
        }
        System.out.println("[CodeGenerator] LLVM file generated as "+fileName);
    }

    /**
     * Show the Llvm result in the standard ouptut
     */
    public void showResult(){
        System.out.println(codeResult.toString());
    }

    /**
     * Set the name of the produced llvm file
     * @param filename name file of Uber-Fortran code
     */
    public void setFileName(String filename) {
        String name;
        if(filename.lastIndexOf('.') > filename.lastIndexOf(File.separatorChar)){
            name = filename.substring(0, filename.lastIndexOf('.'));
        }else{
            name = filename;
        }
        this.fileName = name + ".ll";
    }

    /**
     * add start definition of the main function in llvm file
     */
    public void startMainFunction() {
        this.codeResult.append("define i32 @main(){\n");
    }

    /**
     * add close definition of the main function in llvm file
     */
    public void endMainFunction() {
        this.codeResult.append("\tret i32 0\n}\n");
    }

    /**
     * Declare variable in llvm file
     * @param varName name variable
     */
    public void declareVariable(String varName) {
        String codeRes = "\t%_" + varName + " = alloca i32\n";
        this.codeResult.append(codeRes);
    }

    /**
     * add assignement of a variable in llvm file
     * @param tempVar temporary variable containing value
     * @param assignVar variable to be assigned
     */
    public void assignStatement(String tempVar, String assignVar) {
        String codeRes = "\tstore i32 %" + tempVar + ", i32* %_" + assignVar + "\n";
        this.codeResult.append(codeRes);
    }

    /**
     * add plus statement in llvm file
     * @param tempVar temporary variable where result is stored
     * @param firstOperand first operand of plus statement
     * @param secondOperand second operand of plus statement
     */
    public void plusStatement(String tempVar, String firstOperand, String secondOperand) {
        String codeRes = "\t%" + tempVar + " = add i32 %" + firstOperand + ", %" + secondOperand + "\n";
        this.codeResult.append(codeRes);
    }

    /**
     * add minus statement in llvm file
     * @param tempVar temporary variable where result is stored
     * @param firstOperand first operand of plus statement
     * @param secondOperand second operand of plus statement
     */
    public void minusStatement(String tempVar, String firstOperand, String secondOperand) {
        String codeRes = "\t%" + tempVar + " = sub i32 %" + firstOperand + ", %" + secondOperand + "\n";
        this.codeResult.append(codeRes);
    }

    /**
     * add times statement in llvm file
     * @param tempVar temporary variable where result is stored
     * @param firstOperand first operand of plus statement
     * @param secondOperand second operand of plus statement
     */
    public void timesStatement(String tempVar, String firstOperand, String secondOperand) {
        String codeRes = "\t%" + tempVar + " = mul i32 %" + firstOperand + ", %" + secondOperand + "\n";
        this.codeResult.append(codeRes);
    }

    /**
     * add divide statement in llvm file
     * @param tempVar temporary variable where result is stored
     * @param firstOperand first operand of plus statement
     * @param secondOperand second operand of plus statement
     */
    public void devideStatement(String tempVar, String firstOperand, String secondOperand) {
        String codeRes = "\t%" + tempVar + " = sdiv i32 %" + firstOperand + ", %" + secondOperand + "\n";
        this.codeResult.append(codeRes);
    }

    /**
     * Create temporary variable containing copy of a declared variable in llvm file
     * @param tempVar temporary variable
     * @param codeVariable declared variable
     */
    public void createCopyVarStatement(String tempVar, String codeVariable) {
        String codeRes = "\t%" + tempVar + " = load i32, i32* %" + codeVariable + "\n";
        this.codeResult.append(codeRes);
    }

    /**
     * Create temporary variable containing a number in llvm file
     * @param tempVar temporary variable
     * @param numberValue number
     */
    public void createNumberStatement(String tempVar, String numberValue) {
        String codeRes = "\t%" + tempVar + " = add i32 0, " + numberValue + "\n";
        this.codeResult.append(codeRes);
    }

    /**
     * Create temporary variable containing a negative number in llvm file
     * @param tempVar temporary variable
     * @param negativeNumber negative number
     */
    public void createNegativeNumberStatement(String tempVar, String negativeNumber) {
        String codeRes = "\t%" + tempVar + " = sub i32 0, %" + negativeNumber + "\n";
        this.codeResult.append(codeRes);
    }

    /**
     * Create if condition statement in llvm file
     * @param boolVar temporary variable containing boolean result
     * @param condIdentifier identifier of the if condition in llvm file (label match)
     */
    public void addIfCondition(String boolVar, int condIdentifier) {
        String codeRes = "\tbr i1 %" + boolVar + " , label %if" + condIdentifier + " , label %else_or_endif" + condIdentifier + "\n";
        codeRes += "if" + condIdentifier + ":\n";
        this.codeResult.append(codeRes);
    }

    /**
     * Create Endif condition statement in llvm file
     * @param condIdentifier identifier of the if condition in llvm file (label match)
     */
    public void addElseCondition(int condIdentifier) {
        String codeRes = "\tbr label %else_or_endif" + condIdentifier + "\n";
        codeRes += "else_or_endif" + condIdentifier + ":\n";
        this.codeResult.append(codeRes);
    }

    /**
     * Create Else condition statement in llvm file
     * @param condIdentifier identifier of the if condition in llvm file (label match)
     */
    public void addEndifElseCondition(int condIdentifier) {
        String codeRes = "\tbr label %endif" + condIdentifier + "\n";
        codeRes += "else_or_endif" + condIdentifier + ":\n";
        this.codeResult.append(codeRes);
    }

    /**
     * Create EndifElse condition statement in llvm file
     * @param condIdentifier identifier of the if condition in llvm file (label match)
     */
    public void addEndifCondition(int condIdentifier) {
        String codeRes = "\tbr label %endif" + condIdentifier + "\n";
        codeRes += "endif" + condIdentifier + ":\n";
        this.codeResult.append(codeRes);
    }

    /**
     * Create binary Or statement in llvm file
     * @param tempVar temporary variable where result is stored
     * @param firstOperand first operand of binary Or statement
     * @param secondOperand second operand of binary Or statement
     */
    public void addOrStatement(String tempVar, String firstOperand, String secondOperand) {
        String codeRes = "\t%" + tempVar + " = or i1 %" + firstOperand + ", %" + secondOperand + "\n";
        this.codeResult.append(codeRes);
    }

    /**
     * Create binary And statement in llvm file
     * @param tempVar temporary variable where result is stored
     * @param firstOperand first operand of binary And statement
     * @param secondOperand second operand of binary And statement
     */
    public void addAndStatement(String tempVar, String firstOperand, String secondOperand) {
        String codeRes = "\t%" + tempVar + " = and i1 %" + firstOperand + ", %" + secondOperand + "\n";
        this.codeResult.append(codeRes);
    }

    /**
     * Create unary Not statement in llvm file
     * @param tempVar temporary variable where result is stored
     * @param operand operand of binary Not statement
     */
    public void addNotStatement(String tempVar, String operand) {
        String codeRes = "\t%" + tempVar + " = add i1 %" + operand + ", 1\n";
        this.codeResult.append(codeRes);
    }

    /**
     * Create a condition statement in llvm file
     * @param tempVar temporary variable where result is stored
     * @param compartor comparison operator in the llvm file
     * @param firstOperand first operand of the condition statement
     * @param secondOperand second operand of the condition statement
     */
    public void addSimpleCond(String tempVar, String compartor, String firstOperand, String secondOperand) {
        String codeRes = "\t%" + tempVar + " = icmp " + compartor + " i32 %" + firstOperand + ", %" + secondOperand + "\n";
        this.codeResult.append(codeRes);
    }

    /**
     * Create while condition statement in llvm file
     * @param cond temporary variable containing boolean result
     * @param loopIdentifier identifier of the loop condition in llvm file (label match)
     */
    public void addWhileStatement(String cond,int loopIdentifier){
        String codeRes = "\tbr i1 %" + cond + " , label %while" + loopIdentifier + " , label %do" + loopIdentifier + "\n";
        codeRes += "while" + loopIdentifier + ":\n";
        this.codeResult.append(codeRes);
    }

    /**
     * Create Endif condition statement in llvm file
     * @param loopIdentifier identifier of the if condition in llvm file (label match)
     */
    public void endWhileStatement(int loopIdentifier){
        String codeRes = "\tbr label %end_while" + loopIdentifier + "\n";
        codeRes += "end_while" + loopIdentifier + ":\n";
        this.codeResult.append(codeRes);
    }

    /**
     * Create for statement in llvm file
     * @param iterator declared variable as iterator of the for statement
     * @param initNumber start number of the iterator
     * @param endNumber end number of the iterator
     * @param tempVar1 temporary variable used in the for statement in llvm file
     * @param tempVar2 temporary variable used in the for statement in llvm file
     * @param loopIdentifier identifier of the for loop in llvm file (label match)
     */
    public void addForStatement(String iterator, String initNumber, String endNumber, String tempVar1, String tempVar2, int loopIdentifier) {
        String codeRes = "\tstore i32 " + initNumber + " , i32 * %_" + iterator + "\n";
        codeRes += "\tbr label %loop_for" + loopIdentifier + "\n";
        codeRes += "loop_for" + loopIdentifier + ":\n";
        codeRes += "\t%" + tempVar1 + " = load i32, i32 * %_" + iterator + "\n";
        codeRes += "\t%" + tempVar2 + " = icmp sle i32 %" + tempVar1 + " , " + endNumber + "\n";
        codeRes += "\tbr i1 %" + tempVar2 + " , label %inner_for_loop" + loopIdentifier + " , label %after_do_loop" + loopIdentifier + "\n";
        codeRes += "inner_for_loop" + loopIdentifier + ":\n";
        this.codeResult.append(codeRes);
    }


    /**
     * Create end of a for statement in llvm file
     * @param iterator declared variable as iterator for the do statement
     * @param tempVar1 temporary variable used in the for statement in llvm file
     * @param tempVar2 temporary variable used in the for statement in llvm file
     * @param addVar increment variable used in the for statement in llvm file
     * @param loopIdentifier identifier of the for loop in llvm file (label match)
     */
    public void addEndForByStatement(String iterator, String tempVar1, String tempVar2, String addVar, int loopIdentifier) {
        String codeRes = "\tbr label %end_for_loop" + loopIdentifier + "\n";
        codeRes += "end_for_loop" + loopIdentifier + ":\n";
        codeRes += "\t%" + tempVar1 + " = load i32, i32* %_" + iterator + "\n";
        codeRes += "\t%" + tempVar2 + " = add i32 %" + tempVar1 + " ," + addVar+ "\n"; // Le addVar ici pour inc de addVar
        codeRes += "\tstore i32 %" + tempVar2 + " , i32 * %_" + iterator + "\n";
        codeRes += "\tbr label %loop_for" + loopIdentifier + "\n";
        codeRes += "after_for_loop" + loopIdentifier + ":\n";
        this.codeResult.append(codeRes);
    }

    /**
     * Create end of a for statement in llvm file
     * @param iterator declared variable as iterator of the for statement
     * @param tempVar1 temporary variable used in the for statement in llvm file
     * @param tempVar2 temporary variable used in the for statement in llvm file
     * @param loopIdentifier identifier of the for loop in llvm file (label match)
     */
    public void addEndForStatement(String iterator, String tempVar1, String tempVar2, int loopIdentifier) {
        String codeRes = "\tbr label %end_for_loop" + loopIdentifier + "\n";
        codeRes += "end_for_loop" + loopIdentifier + ":\n";
        codeRes += "\t%" + tempVar1 + " = load i32, i32* %_" + iterator + "\n";
        codeRes += "\t%" + tempVar2 + " = add i32 %" + tempVar1 + " , 1\n"; // Le 1 ici pour inc de 1
        codeRes += "\tstore i32 %" + tempVar2 + " , i32 * %_" + iterator + "\n";
        codeRes += "\tbr label %loop_for" + loopIdentifier + "\n";
        codeRes += "after_for_loop" + loopIdentifier + ":\n";
        this.codeResult.append(codeRes);
    }

    /**
     * Create read statement in llvm file (add function read if needed)
     * @param tempVar temporary variable used during read
     * @param storeVar declared variable where value readed is stored
     */
    public void addReadStatement(String tempVar, String storeVar) {
        if (!funcReadAdded) {
            this.codeResult.insert(0, funcRead);
            funcReadAdded = true;
        }
        String codeRes = "\t%" + tempVar + " = call i32 @readInt ()\n";
        codeRes += "\tstore i32 %" + tempVar + " , i32 * %_" + storeVar + "\n";
        this.codeResult.append(codeRes);
    }

    /**
     * Create print statement in llvm file (add function print if needed)
     * @param tempVar temporary variable used during print
     * @param varPrint declared variable to read
     */
    public void addPrintStatement(String tempVar, String varPrint) {
        if (!funcPrintAdded) {
            this.codeResult.insert(0, funcPrint);
            funcPrintAdded = true;
        }
        String codeRes = "\t%" + tempVar + " = call i32 (i8*, ...) @printf"
                + "(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @string, i32 0, i32 0), i32 %" + varPrint + ")\n";
        this.codeResult.append(codeRes);
    }
}
